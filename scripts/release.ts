/**
 * This script builds to distribuition
 */
import * as appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs-extra';
import { exec } from '../utils/exec';

// First clear the build output dir.
exec(`rimraf ${pathResolve(appRootDir.get(), 'dist')}`);
// Build typescript
exec('tsc');
// Copy documents
fs.copySync(
  pathResolve(appRootDir.get(), 'README.md'),
  pathResolve(appRootDir.get(), 'dist/README.md'),
);
// Clear our package json for release
const packageJson = require('../package.json');
packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson['lint-staged'];
delete packageJson.jest;
delete packageJson.devDependencies;

fs.writeFileSync(
  pathResolve(appRootDir.get(), './dist/package.json'),
  JSON.stringify(packageJson, null, 2),
);
// Copy .gitignore
fs.copySync(
  pathResolve(appRootDir.get(), '.gitignore'),
  pathResolve(appRootDir.get(), 'dist/.gitignore'),
);
