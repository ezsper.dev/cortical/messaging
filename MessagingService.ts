import { BaseContext } from '@cortical/core';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { Redis, RedisOptions } from 'ioredis';

export interface Subscription<T> extends AsyncIterableIterator<T> {
  id: number;
  unsubscribe(): Promise<void>;
}

export interface MessagingOptions {
  connection?: RedisOptions,
  publisher?: Redis;
  subscriber?: Redis;
}

export class MessagingService<C extends BaseContext> {

  constructor(protected context: C, protected options: MessagingOptions = {}) {}

  clientPromise: Promise<RedisPubSub>;

  protected destroyHandler: any;

  connect(): Promise<RedisPubSub> {
    if (this.clientPromise != null) {
      return this.clientPromise;
    }
    this.clientPromise = Promise.resolve(new RedisPubSub(this.options));
    if (typeof this.destroyHandler !== 'undefined') {
      this.destroyHandler = () => {
        this.disconnect()
          .catch(error => console.log(error.stack));
      };
      this.context.destroy(this.destroyHandler);
    }
    return this.clientPromise;
  }

  async disconnect(): Promise<void> {
    const { clientPromise } = this;
    if (typeof clientPromise !== 'undefined') {
      delete this.clientPromise;
      const client = await clientPromise;
      client.getPublisher().quit();
      client.getPublisher().quit();
    }
  }

  async publish<T extends any = any>(name: string, payload: T): Promise<void> {
    const client = await this.connect();
    await client.publish(name, payload);
  }

  async subscribe<T extends any = any>(name: string, onMessage: (payload: T) => any, options?: Object) {
    const client = await this.connect();
    const subId = await client.subscribe(name, onMessage, options);
    let unsubscribed = false;
    const unsubscribe = async () => {
      unsubscribed = true;
      await client.unsubscribe(subId);
    };
    this.context.end(async () => {
      if (!unsubscribed) {
        await unsubscribe();
      }
    });
    return unsubscribe;
  }

  async *asyncIterator<T extends any = any>(name: string | string[]): AsyncIterableIterator<T> {
    const client = await this.connect();
    const iterator = client.asyncIterator<T>(name);
    const asyncIterable = {
      ...iterator,
      [Symbol.asyncIterator]: () => iterator,
    };
    for await (const result of asyncIterable) {
      yield result;
    }
  }

}
