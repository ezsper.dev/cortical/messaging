# CortexQL DataLoader

This is a simple CortexQL wrapper for Facebook's DataLoader

## Usage

In your `api` directory create the directory `loaders`

```
# api/loaders/UserLoader.ts

import { DataLoader } from '@cortexql/dataloader';
import { User } from '../types/User';

export type Key = Pick<User, 'id'>;

export class UserLoader extends DataLoader<Key, User> {

  // batch first
  batchLoad(keys: Key[]) {
    return Promise.all(
      keys.map(key => User.findOne(key)),
    );
  }

}
```

In your context file `api/context/index.ts`

```
import { BaseContext } from '@cortexql/core';
import { UserLoader } from '../loaders/UserLoader';

export class Context extends BaseContext {
  userLoader = new UserLoader(this);
}

```

In whatever resolver `api/query/user.ts`

```
import { Context } from '../context';
import { User } from '../types/User';

export type Arguments {
  id: User['id'];
}

export async function resolveUser(args: Arguments, context: Context) {
  return await context.userLoader.load({ id: args.id });
}

```
