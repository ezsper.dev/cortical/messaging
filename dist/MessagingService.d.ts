import { BaseContext } from '@cortical/core';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import { Redis, RedisOptions } from 'ioredis';
export interface Subscription<T> extends AsyncIterableIterator<T> {
    id: number;
    unsubscribe(): Promise<void>;
}
export interface MessagingOptions {
    connection?: RedisOptions;
    publisher?: Redis;
    subscriber?: Redis;
}
export declare class MessagingService<C extends BaseContext> {
    protected context: C;
    protected options: MessagingOptions;
    constructor(context: C, options?: MessagingOptions);
    clientPromise: Promise<RedisPubSub>;
    protected destroyHandler: any;
    connect(): Promise<RedisPubSub>;
    disconnect(): Promise<void>;
    publish<T extends any = any>(name: string, payload: T): Promise<void>;
    subscribe<T extends any = any>(name: string, onMessage: (payload: T) => any, options?: Object): Promise<() => Promise<void>>;
    asyncIterator<T extends any = any>(name: string | string[]): AsyncIterableIterator<T>;
}
