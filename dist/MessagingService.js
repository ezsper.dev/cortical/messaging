"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __await = (this && this.__await) || function (v) { return this instanceof __await ? (this.v = v, this) : new __await(v); }
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __asyncGenerator = (this && this.__asyncGenerator) || function (thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_redis_subscriptions_1 = require("graphql-redis-subscriptions");
class MessagingService {
    constructor(context, options = {}) {
        this.context = context;
        this.options = options;
    }
    connect() {
        if (this.clientPromise != null) {
            return this.clientPromise;
        }
        this.clientPromise = Promise.resolve(new graphql_redis_subscriptions_1.RedisPubSub(this.options));
        if (typeof this.destroyHandler !== 'undefined') {
            this.destroyHandler = () => {
                this.disconnect()
                    .catch(error => console.log(error.stack));
            };
            this.context.destroy(this.destroyHandler);
        }
        return this.clientPromise;
    }
    disconnect() {
        return __awaiter(this, void 0, void 0, function* () {
            const { clientPromise } = this;
            if (typeof clientPromise !== 'undefined') {
                delete this.clientPromise;
                const client = yield clientPromise;
                client.getPublisher().quit();
                client.getPublisher().quit();
            }
        });
    }
    publish(name, payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield this.connect();
            yield client.publish(name, payload);
        });
    }
    subscribe(name, onMessage, options) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield this.connect();
            const subId = yield client.subscribe(name, onMessage, options);
            let unsubscribed = false;
            const unsubscribe = () => __awaiter(this, void 0, void 0, function* () {
                unsubscribed = true;
                yield client.unsubscribe(subId);
            });
            this.context.end(() => __awaiter(this, void 0, void 0, function* () {
                if (!unsubscribed) {
                    yield unsubscribe();
                }
            }));
            return unsubscribe;
        });
    }
    asyncIterator(name) {
        return __asyncGenerator(this, arguments, function* asyncIterator_1() {
            var e_1, _a;
            const client = yield __await(this.connect());
            const iterator = client.asyncIterator(name);
            const asyncIterable = Object.assign({}, iterator, { [Symbol.asyncIterator]: () => iterator });
            try {
                for (var asyncIterable_1 = __asyncValues(asyncIterable), asyncIterable_1_1; asyncIterable_1_1 = yield __await(asyncIterable_1.next()), !asyncIterable_1_1.done;) {
                    const result = asyncIterable_1_1.value;
                    yield yield __await(result);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (asyncIterable_1_1 && !asyncIterable_1_1.done && (_a = asyncIterable_1.return)) yield __await(_a.call(asyncIterable_1));
                }
                finally { if (e_1) throw e_1.error; }
            }
        });
    }
}
exports.MessagingService = MessagingService;
//# sourceMappingURL=MessagingService.js.map